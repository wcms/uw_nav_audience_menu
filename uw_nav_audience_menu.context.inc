<?php

/**
 * @file
 * uw_nav_audience_menu.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_nav_audience_menu_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'audience_menu';
  $context->description = 'Menu placement';
  $context->tag = 'Navigation';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-audience-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-audience-menu',
          'region' => 'sidebar_first',
          'weight' => '0',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Menu placement');
  t('Navigation');
  $export['audience_menu'] = $context;

  return $export;
}
