<?php

/**
 * @file
 * uw_nav_audience_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function uw_nav_audience_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-audience-menu.
  $menus['menu-audience-menu'] = array(
    'menu_name' => 'menu-audience-menu',
    'title' => 'Information for',
    'description' => 'Menu for pages targeted towards a specific audience (current/future students, faculty, alumni, staff...)',
    'language' => 'und',
    'i18n_mode' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Information for');
  t('Menu for pages targeted towards a specific audience (current/future students, faculty, alumni, staff...)');

  return $menus;
}
