<?php

/**
 * @file
 * uw_nav_audience_menu.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_nav_audience_menu_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
